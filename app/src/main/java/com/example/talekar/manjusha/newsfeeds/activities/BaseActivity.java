package com.example.talekar.manjusha.newsfeeds.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.talekar.manjusha.newsfeeds.R;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }
}
