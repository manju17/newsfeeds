package com.example.talekar.manjusha.newsfeeds.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataItems implements Parcelable {
    @SerializedName("place_of_publication")
    private String placeOfPublication;

    @SerializedName("start_year")
    private String startYear;

    @SerializedName("publisher")
    private String publisher;

    @SerializedName("url")
    private String url;

    @SerializedName("title")
    private String title;

    @SerializedName("end_year")
    private String endYear;

    @SerializedName("country")
    private String country;

    @SerializedName("frequency")
    private String frequency;

    @SerializedName("subjects")
    private List<String> subjects;

    @SerializedName("language")
    private List<String> language;

    @SerializedName("note")
    private List<String> note;

    @SerializedName("lccn")
    private String lccn;

    @SerializedName("id")
    private String id;

    @SerializedName("oclc")
    private String oclc;

    public String getFrequency() {
        return frequency;
    }

    public String getLccn() {
        return lccn;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public List<String> getLanguage() {
        return language;
    }

    public String getCountry() {
        return country;
    }

    public String getPlaceOfPublication() {
        return placeOfPublication;
    }

    public String getStartYear() {
        return startYear;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getOclc() {
        return oclc;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getEndYear() {
        return endYear;
    }

    public List<String> getNote() {
        return note;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.placeOfPublication);
        dest.writeString(this.startYear);
        dest.writeString(this.publisher);
        dest.writeString(this.url);
        dest.writeString(this.title);
        dest.writeString(this.endYear);
        dest.writeString(this.country);
        dest.writeString(this.frequency);
        dest.writeStringList(this.subjects);
        dest.writeStringList(this.language);
        dest.writeStringList(this.note);
        dest.writeString(this.lccn);
        dest.writeString(this.oclc);
        dest.writeString(this.id);
    }

    public DataItems() {
    }

    protected DataItems(Parcel in) {
        this.placeOfPublication = in.readString();
        this.startYear = in.readString();
        this.publisher = in.readString();
        this.url = in.readString();
        this.title = in.readString();
        this.endYear = in.readString();
        this.country = in.readString();
        this.frequency = in.readString();
        this.subjects = in.createStringArrayList();
        this.language = in.createStringArrayList();
        this.note = in.createStringArrayList();
        this.lccn = in.readString();
        this.oclc = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<DataItems> CREATOR = new Parcelable.Creator<DataItems>() {
        @Override
        public DataItems createFromParcel(Parcel source) {
            return new DataItems(source);
        }

        @Override
        public DataItems[] newArray(int size) {
            return new DataItems[size];
        }
    };
}
