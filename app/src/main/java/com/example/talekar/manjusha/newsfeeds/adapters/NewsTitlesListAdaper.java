package com.example.talekar.manjusha.newsfeeds.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.talekar.manjusha.newsfeeds.R;
import com.example.talekar.manjusha.newsfeeds.model.DataItems;
import com.example.talekar.manjusha.newsfeeds.utility.Util;

import java.util.List;

import static com.example.talekar.manjusha.newsfeeds.utility.Constants.TITLE_FORMAT;

public class NewsTitlesListAdaper extends RecyclerView.Adapter<NewsTitlesListAdaper.ViewHolder> {

    private List<DataItems> dataItemsList;
    private OnItemClickCallback onItemClickCallback;

    public NewsTitlesListAdaper(List<DataItems> dataItemsList, OnItemClickCallback onItemClickCallback) {
        this.dataItemsList = dataItemsList;
        this.onItemClickCallback = onItemClickCallback;
    }

    public void updateDataSet(List<DataItems> dataItemsList) {
        this.dataItemsList = dataItemsList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_titles_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        DataItems dataItems = dataItemsList.get(position);
        holder.tvTitles.setText(String.format(TITLE_FORMAT, dataItems.getTitle(), dataItems.getPlaceOfPublication(), dataItems.getStartYear(), dataItems.getEndYear()));
        holder.tvTitles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickCallback.onItemClick(dataItemsList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return Util.isEmpty(dataItemsList) ? 0 : dataItemsList.size();
    }

    /**
     * View holder to set list item views
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitles;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitles = itemView.findViewById(R.id.tv_title);
        }
    }

    /**
     * Callback to send clicked item to fragment
     */
    public interface OnItemClickCallback {
        /**
         * This is a callback method, gets called when list item gets clicked.
         *
         * @param dataItem - object
         */
        void onItemClick(DataItems dataItem);
    }
}
