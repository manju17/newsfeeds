package com.example.talekar.manjusha.newsfeeds.utility;

public class Constants {

    public static final String TITLE_FORMAT = "%s(%s) %s-%s";
    public static final String DATE_OF_PUBLICATION_FORMAT = "%s-%s";

    //Fragment tags
    public static final String TAG_NEWS_DETAIL_FRAGMENT = "news_detail_fragment";
    public static final String TAG_SEARCH_ARTICAL_FRAGMENT = "search_artical_fragment";
    public static final String TAG_NEWS_DETAIL_LEVEL_2_FRAGMENT = "news_detail_level2_fragment";
    public static final String SEPERATOR_PIPE = " | ";
    public static final String SEPERATOR_NEW_LINE = "\n";
}
