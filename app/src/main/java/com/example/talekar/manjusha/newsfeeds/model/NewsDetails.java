package com.example.talekar.manjusha.newsfeeds.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsDetails implements Parcelable {
    @SerializedName("place_of_publication")
    private String placeOfPublication;
    @SerializedName("lccn")
    private String lccn;
    @SerializedName("start_year")
    private String startYear;
    @SerializedName("place")
    private List<String> place = null;
    @SerializedName("name")
    private String name;
    @SerializedName("publisher")
    private String publisher;
    @SerializedName("url")
    private String url;
    @SerializedName("end_year")
    private String endYear;
    @SerializedName("issues")
    private List<String> issues = null;
    @SerializedName("subject")
    private List<String> subject = null;

    public String getPlaceOfPublication() {
        return placeOfPublication;
    }

    public String getLccn() {
        return lccn;
    }

    public String getStartYear() {
        return startYear;
    }

    public List<String> getPlace() {
        return place;
    }

    public String getName() {
        return name;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getUrl() {
        return url;
    }

    public String getEndYear() {
        return endYear;
    }

    public List<String> getIssues() {
        return issues;
    }

    public List<String> getSubject() {
        return subject;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.placeOfPublication);
        dest.writeString(this.lccn);
        dest.writeString(this.startYear);
        dest.writeStringList(this.place);
        dest.writeString(this.name);
        dest.writeString(this.publisher);
        dest.writeString(this.url);
        dest.writeString(this.endYear);
        dest.writeStringList(this.issues);
        dest.writeStringList(this.subject);
    }

    public NewsDetails() {
    }

    protected NewsDetails(Parcel in) {
        this.placeOfPublication = in.readString();
        this.lccn = in.readString();
        this.startYear = in.readString();
        this.place = in.createStringArrayList();
        this.name = in.readString();
        this.publisher = in.readString();
        this.url = in.readString();
        this.endYear = in.readString();
        this.issues = in.createStringArrayList();
        this.subject = in.createStringArrayList();
    }

    public static final Parcelable.Creator<NewsDetails> CREATOR = new Parcelable.Creator<NewsDetails>() {
        @Override
        public NewsDetails createFromParcel(Parcel source) {
            return new NewsDetails(source);
        }

        @Override
        public NewsDetails[] newArray(int size) {
            return new NewsDetails[size];
        }
    };
}
