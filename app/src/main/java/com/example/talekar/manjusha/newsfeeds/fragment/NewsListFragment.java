package com.example.talekar.manjusha.newsfeeds.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.talekar.manjusha.newsfeeds.R;
import com.example.talekar.manjusha.newsfeeds.adapters.NewsTitlesListAdaper;
import com.example.talekar.manjusha.newsfeeds.listeners.OnFragmentInteractionListener;
import com.example.talekar.manjusha.newsfeeds.model.DataItems;
import com.example.talekar.manjusha.newsfeeds.utility.Util;
import com.example.talekar.manjusha.newsfeeds.utility.NetworkUtils;
import com.example.talekar.manjusha.newsfeeds.viewmodel.NewsListViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.example.talekar.manjusha.newsfeeds.utility.Constants.TAG_SEARCH_ARTICAL_FRAGMENT;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class NewsListFragment extends BaseFragment implements NewsTitlesListAdaper.OnItemClickCallback, SwipeRefreshLayout.OnRefreshListener {

    private OnFragmentInteractionListener listner;
    private NewsListViewModel newsListViewModel;
    private Observer<List<DataItems>> datObserver;
    private List<DataItems> mDataItemsList = new ArrayList<>();

    @InjectView(R.id.rv_titles)
    RecyclerView recyclerView;

    @InjectView(R.id.swipe_to_refresh)
    SwipeRefreshLayout swipeToRefresh;

    @InjectView(R.id.search_articles)
    Button searchArticles;

    @InjectView(R.id.search_news)
    Button searchNews;

    @InjectView(R.id.edt_search)
    EditText searchText;
    private NewsTitlesListAdaper newsTitlesListAdaper;

    public NewsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newsListViewModel = ViewModelProviders.of(this).get(NewsListViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        ButterKnife.inject(this, view);
        swipeToRefresh.setOnRefreshListener(this);
        parentBase = view.findViewById(R.id.parent_news_list);
        showSnackbar(getString(R.string.msg_pull_to_refresh));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecor = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(itemDecor);
        newsTitlesListAdaper = new NewsTitlesListAdaper(mDataItemsList, this);
        recyclerView.setAdapter(newsTitlesListAdaper);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Util.isEmpty(mDataItemsList)) {
            getNewsTitleList(false);
        } else {
            updateList(mDataItemsList);
        }
    }

    @OnClick(R.id.search_articles)
    public void onClick(View v) {
        listner.onSearchArticalInteraction(new SearchArticleFragment(), TAG_SEARCH_ARTICAL_FRAGMENT);
    }

    @OnClick(R.id.search_news)
    public void serchArticle() {
        getNewsTitleList(false);
    }

    private void getNewsTitleList(boolean isPullToRefresh) {
        Util.hideKeyboard(getActivity());
        if (NetworkUtils.isConnectedToNetwork(getContext())) {
            if (!isPullToRefresh)
                showProgressDialog(R.string.fetching_list);

            if (datObserver == null) {
                datObserver = new Observer<List<DataItems>>() {
                    @Override
                    public void onChanged(List<DataItems> dataItemsList) {
                        dismissProgressDialog();
                        mDataItemsList = dataItemsList;
                        if (Util.isEmpty(dataItemsList)) {
                            showAlertDialog(getString(R.string.title_data_error), getString(R.string.msg_data_error));
                        } else {
                            updateList(dataItemsList);
                        }
                    }
                };
            }
            newsListViewModel.getNewsFeeds(searchText.getText().toString()).observe(this, datObserver);
        } else {
            showAlertDialog(getString(R.string.title_network_error), getString(R.string.msg_network_error));
        }
    }

    private void updateList(List<DataItems> dataItemsList) {
        if (swipeToRefresh.isRefreshing())
            swipeToRefresh.setRefreshing(false);
        newsTitlesListAdaper.updateDataSet(dataItemsList);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listner = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listner = null;
    }

    @Override
    public void onItemClick(DataItems dataItem) {
        listner.onListFragmentInteraction(dataItem);
    }

    @Override
    public void onRefresh() {
        getNewsTitleList(true);
    }
}
