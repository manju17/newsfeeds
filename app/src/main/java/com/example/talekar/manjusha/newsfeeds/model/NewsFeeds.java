package com.example.talekar.manjusha.newsfeeds.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsFeeds implements Parcelable {

    @SerializedName("totalItems")
    private String totalItems;

    @SerializedName("endIndex")
    private String endIndex;

    @SerializedName("startIndex")
    private String startIndex;

    @SerializedName("itemsPerPage")
    private String itemsPerPage;

    @SerializedName("items")
    private List<DataItems> dataItemsList;

    public String getTotalItems() {
        return totalItems;
    }

    public String getEndIndex() {
        return endIndex;
    }

    public String getStartIndex() {
        return startIndex;
    }

    public String getItemsPerPage() {
        return itemsPerPage;
    }

    public List<DataItems> getDataItemsList() {
        return dataItemsList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.totalItems);
        dest.writeString(this.endIndex);
        dest.writeString(this.startIndex);
        dest.writeString(this.itemsPerPage);
        dest.writeTypedList(this.dataItemsList);
    }

    public NewsFeeds() {
    }

    protected NewsFeeds(Parcel in) {
        this.totalItems = in.readString();
        this.endIndex = in.readString();
        this.startIndex = in.readString();
        this.itemsPerPage = in.readString();
        this.dataItemsList = in.createTypedArrayList(DataItems.CREATOR);
    }

    public static final Parcelable.Creator<NewsFeeds> CREATOR = new Parcelable.Creator<NewsFeeds>() {
        @Override
        public NewsFeeds createFromParcel(Parcel source) {
            return new NewsFeeds(source);
        }

        @Override
        public NewsFeeds[] newArray(int size) {
            return new NewsFeeds[size];
        }
    };
}
