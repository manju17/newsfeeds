package com.example.talekar.manjusha.newsfeeds.activities;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.talekar.manjusha.newsfeeds.R;
import com.example.talekar.manjusha.newsfeeds.fragment.NewsDetailFragment;
import com.example.talekar.manjusha.newsfeeds.fragment.NewsDetailLevel2Fragment;
import com.example.talekar.manjusha.newsfeeds.fragment.NewsListFragment;
import com.example.talekar.manjusha.newsfeeds.listeners.OnFragmentInteractionListener;
import com.example.talekar.manjusha.newsfeeds.model.DataItems;
import com.example.talekar.manjusha.newsfeeds.model.NewsDetails;

import static com.example.talekar.manjusha.newsfeeds.utility.Constants.TAG_NEWS_DETAIL_FRAGMENT;
import static com.example.talekar.manjusha.newsfeeds.utility.Constants.TAG_NEWS_DETAIL_LEVEL_2_FRAGMENT;

public class HomeActivity extends BaseActivity implements OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setToolbarTitle(getString(R.string.home_page));
        addAndNavigateToFragment(new NewsListFragment());
    }

    private void navigateToSearchPage() {

    }

    /**
     * This method will allow to update toolbar from all child fragments
     *
     * @param title - toolbar title
     */
    public void setToolbarTitle(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        title = TextUtils.isEmpty(title) ? getString(R.string.app_name) : title;
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
    }

    private void addAndNavigateToFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    private void replaceAndNavigateToFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(tag).commit();
    }

    @Override
    public void onListFragmentInteraction(DataItems dataItem) {
        replaceAndNavigateToFragment(NewsDetailFragment.getInstance(dataItem), TAG_NEWS_DETAIL_FRAGMENT);
    }

    @Override
    public void onDetailFragmentInteraction(NewsDetails newsDetails) {
        replaceAndNavigateToFragment(NewsDetailLevel2Fragment.getInstance(newsDetails), TAG_NEWS_DETAIL_LEVEL_2_FRAGMENT);
    }

    @Override
    public void onSearchArticalInteraction(Fragment fragment, String tag) {
        replaceAndNavigateToFragment(fragment,tag);
    }
}
