package com.example.talekar.manjusha.newsfeeds.utility;

public class UrlConstants {
    //API urls
    public static final String API_NEWS_FEEDS = "search/titles/results/";
    public static final String API_NEWS_DETAILS = "lccn/{lccn}.json";
    public static final String API_ARTICALS = "search/pages/results/";

    //Key constants
    public static final String KEY_TERMS = "terms";
    public static final String KEY_FORMAT = "format";
    public static final String VALUE_JSON = "json";
    public static final String KEY_TEXT = "andtext";

    public static final String KEY_STATE = "state";
    public static final String KEY_COUNTRY = "country";
    public static final String KEY_CITY = "city";
    public static final String KEY_YEAR1 = "year1";
    public static final String VALUE_YEAR1 = "1960";
    public static final String KEY_YEAR2 = "year2";
}
