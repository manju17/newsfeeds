package com.example.talekar.manjusha.newsfeeds.listeners;

import androidx.fragment.app.Fragment;

import com.example.talekar.manjusha.newsfeeds.model.DataItems;
import com.example.talekar.manjusha.newsfeeds.model.NewsDetails;

/**
 * This is a contract interface between activity and fragment to communicate.
 */
public interface OnFragmentInteractionListener {

    /**
     * This method will send a selected data item to activity to process.
     *
     * @param dataItem - object
     */
    void onListFragmentInteraction(DataItems dataItem);

    void onDetailFragmentInteraction(NewsDetails newsDetails);
    void onSearchArticalInteraction(Fragment fragment,String tag);
}
