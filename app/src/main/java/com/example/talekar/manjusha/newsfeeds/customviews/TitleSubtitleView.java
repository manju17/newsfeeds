package com.example.talekar.manjusha.newsfeeds.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.example.talekar.manjusha.newsfeeds.R;

import java.util.List;


public class TitleSubtitleView extends LinearLayout {

    private TextView title;
    private TextView subtitle;

    public TitleSubtitleView(Context context) {
        this(context, null);
    }

    public TitleSubtitleView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.titleSubtitleView);
    }

    public TitleSubtitleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // update the layout style, setting it in code to avoid an unnecessary extra layer in layout file
        int left = getResources().getDimensionPixelSize(R.dimen.padding_default);
        int right = getResources().getDimensionPixelSize(R.dimen.padding_default);
        int top = getResources().getDimensionPixelSize(R.dimen.padding_default);
        int bottom = getResources().getDimensionPixelSize(R.dimen.padding_default);
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        setPadding(left, top, right, bottom);

        View view = inflate(getContext(), getLayoutResourceId(), this);
        title = view.findViewById(R.id.custom_view_title);
        subtitle = view.findViewById(R.id.custom_view_subtitle);

        final TypedArray ta = context.obtainStyledAttributes(attrs,
                R.styleable.TitleSubtitleView, defStyle, 0);

        try {

            int titleColor = ta.getColor(R.styleable.TitleSubtitleView_titleTextColor, ContextCompat.getColor(context, android.R.color.black));
            title.setTextColor(titleColor);

            int subtitleColor = ta.getColor(R.styleable.TitleSubtitleView_subtitleTextColor, ContextCompat.getColor(context, android.R.color.black));
            subtitle.setTextColor(subtitleColor);

            String str = ta.getString(R.styleable.TitleSubtitleView_title);
            if (!TextUtils.isEmpty(str)) {
                title.setText(str);
            }

            int textStyleTitle = ta.getInt(R.styleable.TitleSubtitleView_titleViewStyle, Typeface.NORMAL);
            title.setTypeface(title.getTypeface(), textStyleTitle);

            str = ta.getString(R.styleable.TitleSubtitleView_subtitle);
            if (!TextUtils.isEmpty(str)) {
                subtitle.setText(str);
            }

        } finally {
            ta.recycle();
        }

    }

    /**
     * Override this method if you want to use a different layout with different styling.
     *
     * @return
     */
    protected int getLayoutResourceId() {
        return R.layout.view_title_subtitle;
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setSubtitle(String subtitle, boolean fromHtml) {
        if (fromHtml) {
            this.subtitle.setText(Html.fromHtml(subtitle));
        } else {
            this.subtitle.setText(subtitle);
        }
    }

    public void setSubtitle(List<String> subtitleList, String seperater) {
        for (String subtitle : subtitleList) {
            this.subtitle.setText(subtitle + seperater);
        }
    }

    public TextView getTitle() {
        return title;
    }

    public TextView getSubtitle() {
        return subtitle;
    }
}
