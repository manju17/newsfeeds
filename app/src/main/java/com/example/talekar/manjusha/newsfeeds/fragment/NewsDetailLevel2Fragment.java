package com.example.talekar.manjusha.newsfeeds.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.talekar.manjusha.newsfeeds.R;
import com.example.talekar.manjusha.newsfeeds.activities.HomeActivity;
import com.example.talekar.manjusha.newsfeeds.customviews.TitleSubtitleView;
import com.example.talekar.manjusha.newsfeeds.listeners.OnFragmentInteractionListener;
import com.example.talekar.manjusha.newsfeeds.model.NewsDetails;
import com.example.talekar.manjusha.newsfeeds.viewmodel.NewsDetailViewModel;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.example.talekar.manjusha.newsfeeds.utility.Constants.DATE_OF_PUBLICATION_FORMAT;
import static com.example.talekar.manjusha.newsfeeds.utility.Constants.SEPERATOR_PIPE;

/**
 * A simple {@link Fragment} subclass to show news details.
 */
public class NewsDetailLevel2Fragment extends BaseFragment {
    public static final String ARG_DATA = "ARG_DATA";

    private NewsDetails newsDetails;
    private OnFragmentInteractionListener listner;

    @InjectView(R.id.name)
    TitleSubtitleView name;

    @InjectView(R.id.place_of_publication)
    TitleSubtitleView placeOfPublication;

    @InjectView(R.id.places)
    TitleSubtitleView places;

    @InjectView(R.id.publisher)
    TitleSubtitleView publisher;

    @InjectView(R.id.dates_of_publication)
    TitleSubtitleView datesOfPublication;

    @InjectView(R.id.llcn)
    TitleSubtitleView llcn;

    private NewsDetailViewModel newsDetailsViewModel;
    private Observer<NewsDetails> datObserver;

    public NewsDetailLevel2Fragment() {
        // Required empty public constructor
    }

    public static NewsDetailLevel2Fragment getInstance(NewsDetails newsDetails) {
        NewsDetailLevel2Fragment newsDetailFragment = new NewsDetailLevel2Fragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_DATA, newsDetails);
        newsDetailFragment.setArguments(bundle);
        return newsDetailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != getArguments()) {
            newsDetails = getArguments().getParcelable(ARG_DATA);
        }
        ((HomeActivity) getActivity()).setToolbarTitle(getString(R.string.news_detail_page));
        newsDetailsViewModel = ViewModelProviders.of(this).get(NewsDetailViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_detail_level2, container, false);
        ButterKnife.inject(this, view);
        bindDataToView();
        return view;
    }

    private void bindDataToView() {
        name.setTitle(getString(R.string.name));
        name.setSubtitle(newsDetails.getName(), false);

        placeOfPublication.setTitle(getString(R.string.place_of_publication));
        placeOfPublication.setSubtitle(newsDetails.getPlaceOfPublication(), false);

        publisher.setTitle(getString(R.string.publisher));
        publisher.setSubtitle(newsDetails.getPublisher(), false);

        datesOfPublication.setTitle(getString(R.string.dates_of_publication));
        datesOfPublication.setSubtitle(String.format(DATE_OF_PUBLICATION_FORMAT, newsDetails.getStartYear(), newsDetails.getEndYear()), false);

        llcn.setTitle(getString(R.string.llcn));
        llcn.setSubtitle(newsDetails.getLccn(), false);

        places.setTitle(getString(R.string.places));
        places.setSubtitle(newsDetails.getPlace(), SEPERATOR_PIPE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listner = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listner = null;
    }
}