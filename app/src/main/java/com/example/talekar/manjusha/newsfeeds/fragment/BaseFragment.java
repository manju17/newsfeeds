package com.example.talekar.manjusha.newsfeeds.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.example.talekar.manjusha.newsfeeds.R;
import com.google.android.material.snackbar.Snackbar;

/**
 * A placeholder fragment containing a simple view.
 */
public class BaseFragment extends Fragment {

    private ProgressDialog progressDialog;
    private AlertDialog.Builder builder;
    public CoordinatorLayout parentBase;

    public BaseFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base, container, false);
        return view;
    }

    /**
     * This method will show a progress dialog
     *
     * @param stringRes - caption for a progress bar
     */
    public void showProgressDialog(int stringRes) {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        }
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(stringRes));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing() && isAdded()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    /**
     * This method will show a alert dialog
     *
     * @param title - title of dialog
     * @param msg   - message of dialog
     */
    public void showAlertDialog(String title, String msg) {
        builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog);
        builder.setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void showSnackbar(String msg) {
        Snackbar snackbar = Snackbar.make(parentBase, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
