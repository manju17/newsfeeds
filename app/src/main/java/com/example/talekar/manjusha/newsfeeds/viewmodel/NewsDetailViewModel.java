package com.example.talekar.manjusha.newsfeeds.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.talekar.manjusha.newsfeeds.model.NewsDetails;
import com.example.talekar.manjusha.newsfeeds.services.ApiClient;
import com.example.talekar.manjusha.newsfeeds.services.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class NewsDetailViewModel extends ViewModel {
    private MutableLiveData<NewsDetails> newsDetailsLiveData = new MutableLiveData<>();

    public LiveData<NewsDetails> getNewsDetails(String lccn) {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        apiService.getNewsDetails(lccn)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<NewsDetails>() {
                    @Override
                    public void onSuccess(NewsDetails newsDetails) {
                        newsDetailsLiveData.postValue(newsDetails);
                    }

                    @Override
                    public void onError(Throwable e) {
                        newsDetailsLiveData.postValue(null);
                    }
                });

        return newsDetailsLiveData;
    }


}
