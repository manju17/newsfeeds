package com.example.talekar.manjusha.newsfeeds.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.talekar.manjusha.newsfeeds.R;
import com.example.talekar.manjusha.newsfeeds.adapters.ArticalTitleAdapter;
import com.example.talekar.manjusha.newsfeeds.listeners.OnFragmentInteractionListener;
import com.example.talekar.manjusha.newsfeeds.model.DataItems;
import com.example.talekar.manjusha.newsfeeds.utility.NetworkUtils;
import com.example.talekar.manjusha.newsfeeds.utility.Util;
import com.example.talekar.manjusha.newsfeeds.viewmodel.NewsDetailViewModel;
import com.example.talekar.manjusha.newsfeeds.viewmodel.NewsListViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class SearchArticleFragment extends BaseFragment implements ArticalTitleAdapter.OnItemClickCallback, SwipeRefreshLayout.OnRefreshListener {

    private OnFragmentInteractionListener listner;
    private NewsListViewModel newsListViewModel;
    private Observer<List<DataItems>> datObserver;
    private List<DataItems> mDataItemsList = new ArrayList<>();

    @InjectView(R.id.artical_titles)
    RecyclerView recyclerView;

    @InjectView(R.id.swipe_to_refresh)
    SwipeRefreshLayout swipeToRefresh;

    @InjectView(R.id.search_artical)
    Button searchArticles;

    @InjectView(R.id.edt_search)
    EditText searchText;

    private ArticalTitleAdapter articalTitleAdapter;

    public SearchArticleFragment() {
        // Required empty public constructor
    }

    public static SearchArticleFragment newInstance() {
        SearchArticleFragment fragment = new SearchArticleFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newsListViewModel = ViewModelProviders.of(this).get(NewsListViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_article, container, false);
        ButterKnife.inject(this, view);
        swipeToRefresh.setOnRefreshListener(this);
        parentBase = view.findViewById(R.id.parent_artical_list);
        showSnackbar(getString(R.string.msg_pull_to_refresh));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecor = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(itemDecor);
        articalTitleAdapter = new ArticalTitleAdapter(mDataItemsList, this);
        recyclerView.setAdapter(articalTitleAdapter);
        return view;
    }

    @OnClick(R.id.search_artical)
    public void serchArticle() {
        getArticalTitleList(false);
    }


    private void getArticalTitleList(boolean isPullToRefresh) {
        Util.hideKeyboard(getActivity());
        if (NetworkUtils.isConnectedToNetwork(getContext())) {
            if (!isPullToRefresh)
                showProgressDialog(R.string.fetching_list);

            if (datObserver == null) {
                datObserver = new Observer<List<DataItems>>() {
                    @Override
                    public void onChanged(List<DataItems> dataItemsList) {
                        dismissProgressDialog();
                        mDataItemsList = dataItemsList;
                        if (Util.isEmpty(dataItemsList)) {
                            showAlertDialog(getString(R.string.title_data_error), getString(R.string.msg_data_error));
                        } else {
                            updateList(dataItemsList);
                        }
                    }
                };
            }
            newsListViewModel.getArticals(searchText.getText().toString()).observe(this, datObserver);

        } else {
            showAlertDialog(getString(R.string.title_network_error), getString(R.string.msg_network_error));
        }
    }

    private void updateList(List<DataItems> dataItemsList) {
        if (swipeToRefresh.isRefreshing())
            swipeToRefresh.setRefreshing(false);
        articalTitleAdapter.updateDataSet(dataItemsList);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listner = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listner = null;
    }

    @Override
    public void onItemClick(DataItems dataItem) {
    }


    @Override
    public void onRefresh() {
        getArticalTitleList(true);
    }
}
