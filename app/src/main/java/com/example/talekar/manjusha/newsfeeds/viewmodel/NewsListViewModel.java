package com.example.talekar.manjusha.newsfeeds.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.talekar.manjusha.newsfeeds.model.DataItems;
import com.example.talekar.manjusha.newsfeeds.model.NewsFeeds;
import com.example.talekar.manjusha.newsfeeds.services.ApiClient;
import com.example.talekar.manjusha.newsfeeds.services.ApiService;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.KEY_FORMAT;
import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.KEY_TERMS;
import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.KEY_TEXT;
import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.KEY_YEAR1;
import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.KEY_YEAR2;
import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.VALUE_JSON;
import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.VALUE_YEAR1;


public class NewsListViewModel extends ViewModel {
    private MutableLiveData<List<DataItems>> newsFeedsLiveData = new MutableLiveData<>();
    private MutableLiveData<List<DataItems>> newsArticalsLiveData = new MutableLiveData<>();

    public LiveData<List<DataItems>> getNewsFeeds(String keyword) {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        apiService.getNewsFeeds(createRequestParams(keyword))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<NewsFeeds>() {
                    @Override
                    public void onSuccess(NewsFeeds newsFeeds) {
                        newsFeedsLiveData.postValue(newsFeeds.getDataItemsList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        newsFeedsLiveData.postValue(null);
                    }
                });

        return newsFeedsLiveData;
    }

    private Map<String, String> createRequestParams(String keyword) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put(KEY_TERMS, keyword);
        requestParams.put(KEY_FORMAT, VALUE_JSON);
        requestParams.put(KEY_YEAR1, VALUE_YEAR1);
        requestParams.put(KEY_YEAR2, "" + Calendar.getInstance().get(Calendar.YEAR));
        return requestParams;
    }


    public LiveData<List<DataItems>>getArticals(String keyword){
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        apiService.getArticals(createArticalRequestParams(keyword))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<NewsFeeds>() {
                    @Override
                    public void onSuccess(NewsFeeds newsFeeds) {
                        newsArticalsLiveData.postValue(newsFeeds.getDataItemsList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        newsArticalsLiveData.postValue(null);
                    }
                });
        return newsArticalsLiveData;
    }

    private Map<String, String> createArticalRequestParams(String keyword) {
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put(KEY_TEXT, keyword);
        requestParams.put(KEY_FORMAT, VALUE_JSON);
        return requestParams;
    }
}
