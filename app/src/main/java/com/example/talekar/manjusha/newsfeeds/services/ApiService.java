package com.example.talekar.manjusha.newsfeeds.services;


import com.example.talekar.manjusha.newsfeeds.model.NewsDetails;
import com.example.talekar.manjusha.newsfeeds.model.NewsFeeds;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.API_ARTICALS;
import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.API_NEWS_DETAILS;
import static com.example.talekar.manjusha.newsfeeds.utility.UrlConstants.API_NEWS_FEEDS;

public interface ApiService {

    @GET(API_NEWS_FEEDS)
    Single<NewsFeeds> getNewsFeeds(@QueryMap Map<String, String> params);

    @GET(API_NEWS_DETAILS)
    Single<NewsDetails> getNewsDetails(@Path("lccn") String lccn);

    @GET(API_ARTICALS)
    Single<NewsFeeds> getArticals(@QueryMap Map<String, String> params);


}
