package com.example.talekar.manjusha.newsfeeds.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.talekar.manjusha.newsfeeds.R;
import com.example.talekar.manjusha.newsfeeds.activities.HomeActivity;
import com.example.talekar.manjusha.newsfeeds.customviews.TitleSubtitleView;
import com.example.talekar.manjusha.newsfeeds.listeners.OnFragmentInteractionListener;
import com.example.talekar.manjusha.newsfeeds.model.DataItems;
import com.example.talekar.manjusha.newsfeeds.model.NewsDetails;
import com.example.talekar.manjusha.newsfeeds.utility.NetworkUtils;
import com.example.talekar.manjusha.newsfeeds.utility.Util;
import com.example.talekar.manjusha.newsfeeds.viewmodel.NewsDetailViewModel;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.example.talekar.manjusha.newsfeeds.utility.Constants.DATE_OF_PUBLICATION_FORMAT;
import static com.example.talekar.manjusha.newsfeeds.utility.Constants.SEPERATOR_NEW_LINE;

/**
 * A simple {@link Fragment} subclass to show news details.
 */
public class NewsDetailFragment extends BaseFragment {
    public static final String ARG_DATA = "ARG_DATA";

    private DataItems dataItem;
    private OnFragmentInteractionListener listner;

    @InjectView(R.id.title)
    TitleSubtitleView title;

    @InjectView(R.id.place_of_publication)
    TitleSubtitleView placeOfPublication;

    @InjectView(R.id.geographical_coverage)
    TitleSubtitleView geographicCoverage;

    @InjectView(R.id.publisher)
    TitleSubtitleView publisher;

    @InjectView(R.id.dates_of_publication)
    TitleSubtitleView datesOfPublication;

    @InjectView(R.id.frequency)
    TitleSubtitleView frequency;

    @InjectView(R.id.oclc)
    TitleSubtitleView oclc;

    @InjectView(R.id.lccn)
    TitleSubtitleView lccn;

    @InjectView(R.id.language)
    TitleSubtitleView language;

    @InjectView(R.id.subjects)
    TitleSubtitleView subjects;

    @InjectView(R.id.notes)
    TitleSubtitleView notes;

    @InjectView(R.id.url)
    TitleSubtitleView url;

    private NewsDetailViewModel newsDetailsViewModel;
    private Observer<NewsDetails> datObserver;

    public NewsDetailFragment() {
        // Required empty public constructor
    }

    public static NewsDetailFragment getInstance(DataItems dataItem) {
        NewsDetailFragment newsDetailFragment = new NewsDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_DATA, dataItem);
        newsDetailFragment.setArguments(bundle);
        return newsDetailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != getArguments()) {
            dataItem = getArguments().getParcelable(ARG_DATA);
        }
        ((HomeActivity) getActivity()).setToolbarTitle(getString(R.string.news_detail_page));
        newsDetailsViewModel = ViewModelProviders.of(this).get(NewsDetailViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_detail, container, false);
        ButterKnife.inject(this, view);
        bindDataToView();
        return view;
    }

    private void bindDataToView() {
        title.setTitle(getString(R.string.title));
        title.setSubtitle(dataItem.getTitle(), false);

        placeOfPublication.setTitle(getString(R.string.place_of_publication));
        placeOfPublication.setSubtitle(dataItem.getPlaceOfPublication(), false);

        geographicCoverage.setTitle(getString(R.string.geographic_coverage));
        geographicCoverage.setSubtitle(dataItem.getCountry(), false);

        publisher.setTitle(getString(R.string.publisher));
        publisher.setSubtitle(dataItem.getPublisher(), false);

        datesOfPublication.setTitle(getString(R.string.dates_of_publication));
        datesOfPublication.setSubtitle(String.format(DATE_OF_PUBLICATION_FORMAT, dataItem.getStartYear(), dataItem.getEndYear()), false);

        frequency.setTitle(getString(R.string.frequency));
        frequency.setSubtitle(dataItem.getFrequency(), false);

        lccn.setTitle(getString(R.string.llcn));
        lccn.setSubtitle(dataItem.getLccn(), false);

        oclc.setTitle(getString(R.string.oclc));
        oclc.setSubtitle(dataItem.getOclc(), false);

        notes.setTitle(getString(R.string.notes));
        if (!Util.isEmpty(dataItem.getNote())) {
            notes.setSubtitle(dataItem.getNote(), SEPERATOR_NEW_LINE);
        } else {
            notes.setSubtitle(getString(R.string.msg_data_error), false);
        }

        subjects.setTitle(getString(R.string.subjects));
        if (!Util.isEmpty(dataItem.getSubjects())) {
            subjects.setSubtitle(dataItem.getSubjects(), SEPERATOR_NEW_LINE);
        } else {
            subjects.setSubtitle(getString(R.string.msg_data_error), false);
        }

        language.setTitle(getString(R.string.language));
        if (!Util.isEmpty(dataItem.getLanguage())) {
            language.setSubtitle(dataItem.getLanguage(), SEPERATOR_NEW_LINE);
        } else {
            language.setSubtitle(getString(R.string.msg_data_error), false);
        }

        url.setTitle(getString(R.string.url));
        url.setSubtitle(Util.getClickableLink(dataItem.getUrl()), true);
        url.getSubtitle().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDetails(dataItem.getLccn());
            }
        });
    }

    private void getDetails(String lccn) {
        if (NetworkUtils.isConnectedToNetwork(getContext())) {
            if (datObserver == null) {
                showProgressDialog(R.string.fetching_list);
                datObserver = new Observer<NewsDetails>() {
                    @Override
                    public void onChanged(NewsDetails newsDetails) {
                        dismissProgressDialog();

                        if (null == newsDetails) {
                            showAlertDialog(getString(R.string.title_data_error), getString(R.string.msg_data_error));
                        } else {
                            listner.onDetailFragmentInteraction(newsDetails);
                        }
                    }
                };
            }
            newsDetailsViewModel.getNewsDetails(lccn).observe(this, datObserver);
        } else {
            showAlertDialog(getString(R.string.title_network_error), getString(R.string.msg_network_error));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listner = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listner = null;
    }
}